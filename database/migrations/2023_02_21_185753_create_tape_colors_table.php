<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_door_colors', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('color');
            $table->decimal('price')->unsigned();
            $table->boolean('active')->default(true);
            $table->string('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tape_colors');
    }
};
