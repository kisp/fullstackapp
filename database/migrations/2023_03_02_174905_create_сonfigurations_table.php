<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('сonfigurations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            
            $table->string('paint_color');
            $table->string('tape_color');
            $table->string('handle_color');
            $table->integer('door_width');
            $table->integer('door_height');
            $table->integer('total_price');
            $table->json('selected_accessories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('сonfigurations');
    }
};
