<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class new_seed_donfiguration_for_img extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('outside_door_colors')->delete();
        DB::table('inside_door_colors')->delete();
        DB::table('box_door_colors')->delete();
        DB::table('handle_colors')->delete();
        DB::table('height_doors')->delete();
        DB::table('width_doors')->delete();
        DB::table('openings')->delete();
        DB::table('accessories')->delete();

        $outside_door_colors = [
            [
                'color' => 'Chocolate',
                'price' => '400',
                'image' => '/images/outsideChocolateDoor.jpg',
            ],
            [
                'color' =>'Gray',
                'price' => '500',
                'image' => '/images/outsideGrayDoor.jpg',
            ],
            [
                'color' => 'White',
                'price' => '600',
                'image' => '/images/outsideWhiteDoor.jpg',
            ],
        ];
        $inside_door_colors = [
            [
                'color' => 'Black',
                'price' => '400',
                'image' => '/images/insideBlackDoor.jpg',
            ],
            [
                'color' =>'Brown',
                'price' => '500',
                'image' => '/images/insideBrownDoor.jpg',
            ],
            [
                'color' => 'Concrete',
                'price' => '600',
                'image' => '/images/insideConcreteDoor.jpg',
            ],
            [
                'color' => 'White',
                'price' => '700',
                'image' => '/images/insideWhiteDoor.jpg',
            ],
        ];
        $box_door_colors = [
            [
                'color' => 'Black',
                'price' => '300',
                'image' => '/images/boxWhiteColor.jpg',
            ],
            [
                'color' => 'Chocolate',
                'price' => '180',
                'image' => '/images/boxChocolateColor.jpg',
            ],
            [
                'color' => 'White',
                'price' => '340',
                'image' => '/images/boxWhiteColor.jpg',
            ],
            [
                'color' => 'Brown',
                'price' => '440',
                'image' => '/images/boxBrownColor.jpg',
            ],
        ];
        $handle_colors = [
            [
                'color' => 'Silver',
                'price' => '340',
                'image' => '/images/handlerSilver.jpg',
            ],
            [
                'color' => 'Gold',
                'price' => '300',
                'image' => '/images/handlerGold.jpg',
            ],
            [
                'color' => 'Line',
                'price' => '300',
                'image' => '/images/handlerLine.jpg',
            ],
        ];
        $height_doors = [
            [
                'height' => '220',
                'price' => '2500'
            ],
            [
                'height' => '218',
                'price' => '2300'
            ],
            [
                'height' => '225',
                'price' => '2800'
            ],
            [
                'height' => '215',
                'price' => '2100'
            ],
        ];
        $width_doors = [
            [
                'width' => '880',
                'price' => '2000'
            ],
            [
                'witdh' => '870',
                'price' => '1850'
            ],
            [
                'witdh' => '890',
                'price' => '2150'
            ],
            [
                'width' => '860',
                'price' => '1750'
            ],
        ];
        $openings = [
            [
                'open' => 'Left',
            ],
            [
                'open' => 'Right',
            ],
        ];
        $accessories = [
            [
                'name' => 'outpeepholeAccessory',
                'price' => '2000',
                'image' => '/images/outpeepholeAccessory.jpg',
            ],
            [
                'name' => 'stchlevAccessory',
                'price' => '3000',
                'image' => '/images/stchlevAccessory.jpg',
            ],
        ];


        DB::table('outside_door_colors')->insert($outside_door_colors);
        DB::table('inside_door_colors')->insert($inside_door_colors);
        DB::table('box_door_colors')->insert($box_door_colors);
        DB::table('handle_colors')->insert($handle_colors);
        DB::table('height_doors')->insert($height_doors);
        DB::table('width_doors')->insert($width_doors);
        DB::table('openings')->insert($openings);
        DB::table('accessories')->insert($accessories);
    }
}


