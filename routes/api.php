<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaintColorController;
use App\Models\configuration;

Route::get('/index', [PaintColorController::class, 'index']);

Route::post('/post', [PaintColorController::class, 'store']);

// Route::post('/configurations', function (Request $request) {
//     $Configuration = сonfiguration::create($request->all());

//     return $Configuration;
// });


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


