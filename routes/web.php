<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaintColorController;

Route::get('/{any}', function () {
    return view('app');
})->where('any', '.*');

// Route::get('/index', [PaintColorController::class, 'index']);
// Route::post('/post', [PaintColorController::class, 'store']);


// Route::resource('user', UserController::class);

// Route::get('colors', [PaintColorController::class, 'index']);
// Route::post('colors', [PaintColorController::class, 'store']);
