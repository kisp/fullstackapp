const mix = require('laravel-mix');



mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.sass', 'public/css')
    .copyDirectory('resources/img', 'public/images')
    .vue()
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
    ])
