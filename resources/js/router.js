import { createWebHashHistory, createRouter } from "vue-router";
import home from './pages/home.vue';
import index from './pages/index.vue';
import params from './pages/params.vue';
const routes = [
    {
        path: '/',
        name: 'Home',
        component: home,
    },
    {
        path: '/index',
        name: 'Index',
        component: index,
    },
    {
        path: '/params',
        name: 'Params',
        component: params,
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
