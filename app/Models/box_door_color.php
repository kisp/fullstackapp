<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class box_door_color extends Model
{
    use HasFactory;

    protected $fillable = [
        'color', 'price', 'active', 'image',
    ];
}
