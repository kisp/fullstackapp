<?php

namespace App\Http\Controllers;

use App\Models\outside_door_color;
use App\Models\inside_door_color;
use App\Models\box_door_color;
use App\Models\handleColor;
use App\Models\widthDoor;
use App\Models\heightDoor;
use App\Models\opening;
use App\Models\accessory;
// use App\Models\configuration;
use Illuminate\Http\Request;

class PaintColorController extends Controller
{

    public function index()
    {
        $outside_door_color = outside_door_color::all();
        $inside_door_color = inside_door_color::all();
        $box_door_color = box_door_color::all();
        $colorHandle = handleColor::all();
        $doorWidth = widthDoor::all();
        $doorHeight = heightDoor::all();
        $open = opening::all();
        $acc = accessory::all();
        // $conf = configuration::all();

        $data = array(
            'doorOutsideColors' => $outside_door_color,
            'doorInsideColors' => $inside_door_color,
            'boxColors' => $box_door_color,
            'handleColors' => $colorHandle,
            'doorWidth' => $doorWidth,
            'doorHeight' => $doorHeight,
            'opens' => $open,
            'accessories' => $acc,
            // 'configuration' => $conf,

        );
        return response()->json($data);

    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $product = new  accessory();
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->save();

        return response()->json($product);
    }
}
